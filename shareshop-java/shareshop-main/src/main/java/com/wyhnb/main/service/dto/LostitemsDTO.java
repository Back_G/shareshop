package com.wyhnb.main.service.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.wyhnb.main.entity.Image;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class LostitemsDTO {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "丢失时间")
    private Date lostTime;

    @ApiModelProperty(value = "用户头像")
    private String itemsName;

    @ApiModelProperty(value = "头像")
    private String avatarUrl;

    @ApiModelProperty(value = "用户名")
    private String nickName;

    @ApiModelProperty(value = "发布者openid")
    private String openid;

    @ApiModelProperty(value = "联系微信号")
    private String userWx;

    @ApiModelProperty(value = "是否有偿")
    private Integer isItPaid;

    @ApiModelProperty(value = "加入时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;

    @ApiModelProperty(value = "图片列表")
    @TableField(exist = false)
    private List<Image> imgList;
}
