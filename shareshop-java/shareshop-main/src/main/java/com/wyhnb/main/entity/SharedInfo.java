package com.wyhnb.main.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_shared_info")
@ApiModel(value = "共享物品事件信息", description = "共享")
public class SharedInfo {
    @TableId(type = IdType.ID_WORKER)
    private Long id;

    @ApiModelProperty(value = "加入时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;

    @ApiModelProperty(value = "类型")
    private Integer type;

    @ApiModelProperty(value = "openid")
    private String openid;
}
