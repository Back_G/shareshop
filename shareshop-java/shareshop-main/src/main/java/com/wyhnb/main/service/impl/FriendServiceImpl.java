package com.wyhnb.main.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wyhnb.main.entity.Friend;
import com.wyhnb.main.mapper.FriendMapper;
import com.wyhnb.main.service.IFriendService;
import org.springframework.stereotype.Service;

/**
 * @Author: Back
 * @Mail: 374616676@qq.com
 * @Description:
 */
@Service
public class FriendServiceImpl extends ServiceImpl<FriendMapper, Friend> implements IFriendService {
}
