package com.wyhnb.main.mapper;

import com.wyhnb.main.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Repository
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
