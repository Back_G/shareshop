package com.wyhnb.main.service.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.wyhnb.main.entity.Image;
import com.wyhnb.main.entity.Lostitems;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SharedInfoPageDTO {
    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "用户的id")
    private String openid;

    @ApiModelProperty(value = "信息的类型")
    private String type;

    @ApiModelProperty(value = "标题名称")
    private String title;

    @ApiModelProperty(value = "价格")
    private String price;

    @ApiModelProperty(value = "是否在线交易")
    private String sellOnline;

    @ApiModelProperty(value = "分类")
    private String itemClassification;

    @ApiModelProperty(value = "用户名")
    private String nickname;

    @ApiModelProperty(value = "头像")
    private String avatarUrl;

    @ApiModelProperty(value = "图片列表")
    @TableField(exist = false)
    private List<Image> imgList;

    @ApiModelProperty(value = "是否为发布丢失物品 0否 1是")
    private Integer isLostitems;

    @ApiModelProperty(value = "发布丢失物品")
    private LostitemsDTO lostitems;
}