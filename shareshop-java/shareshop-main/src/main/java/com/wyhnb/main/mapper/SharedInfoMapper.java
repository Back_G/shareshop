package com.wyhnb.main.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyhnb.main.entity.SharedInfo;
import com.wyhnb.main.service.dto.LostitemsDTO;
import com.wyhnb.main.service.dto.SharedInfoPageDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface SharedInfoMapper extends BaseMapper<SharedInfo> {

    Page<SharedInfoPageDTO> selectPageAll(@Param("page") Page<SharedInfoPageDTO> pageDTOPage);

    LostitemsDTO getLostItemsBy(String id);

}
