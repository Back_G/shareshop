package com.wyhnb.main.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wyhnb.main.entity.Idleitems;
import com.wyhnb.main.entity.Image;
import com.wyhnb.main.entity.SharedInfo;
import com.wyhnb.main.mapper.IdleitemsMapper;
import com.wyhnb.main.mapper.SharedInfoMapper;
import com.wyhnb.main.service.IIdleitemsService;
import com.wyhnb.main.service.IImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 发布闲置物品 服务实现类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Service
public class IdleitemsServiceImpl extends ServiceImpl<IdleitemsMapper, Idleitems> implements IIdleitemsService {

    @Autowired
    private IImageService imageService;

    @Autowired
    private IdleitemsMapper idleitemsMapper;

    @Autowired
    private SharedInfoMapper sharedInfoMapper;

    @Override
    public void addItem(Idleitems idleitems) {
        idleitems.setId(null);
        idleitems.setModifyTime(null);
        idleitems.setCreateTime(null);
        this.save(idleitems);
        List<Image> imgList = new ArrayList<>();
        for (Image image : idleitems.getImgList()) {
            image.setRelationId(idleitems.getId());
            image.setRelationType(1);
            imgList.add(image);
        }
        imageService.saveBatch(imgList);

        SharedInfo sharedInfo = new SharedInfo();
        sharedInfo.setId(idleitems.getId());
        sharedInfo.setType(1);
        sharedInfo.setOpenid(idleitems.getOpenid());
        sharedInfoMapper.insert(sharedInfo);

    }

    @Override
    public Page<Idleitems> getItem(Page<Idleitems> page, Idleitems idleitems) {

        return idleitemsMapper.selectPage(page, null);
    }

}
