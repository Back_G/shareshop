package com.wyhnb.main.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyhnb.main.entity.Image;
import com.wyhnb.main.entity.SharedInfo;
import com.wyhnb.main.entity.Sharedrental;
import com.wyhnb.main.mapper.SharedInfoMapper;
import com.wyhnb.main.mapper.SharednetworkMapper;
import com.wyhnb.main.mapper.SharedrentalMapper;
import com.wyhnb.main.service.IImageService;
import com.wyhnb.main.service.ISharedrentalService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 共享租房 服务实现类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Service
public class SharedrentalServiceImpl extends ServiceImpl<SharedrentalMapper, Sharedrental> implements ISharedrentalService {

    @Autowired
    private IImageService imageService;

    @Autowired
    private SharedrentalMapper sharedrentalMapper;

    @Autowired
    private SharedInfoMapper sharedInfoMapper;

    @Override
    public void addRental(Sharedrental sharedrental) {
        sharedrental.setId(null);
        sharedrental.setModifyTime(null);
        sharedrental.setCreateTime(null);
        this.save(sharedrental);
        List<Image> imgList = new ArrayList<>();
        for (Image image : sharedrental.getImgList()) {
            image.setRelationId(sharedrental.getId());
            image.setRelationType(2);
            imgList.add(image);
        }
        imageService.saveBatch(imgList);

        SharedInfo sharedInfo = new SharedInfo();
        sharedInfo.setId(sharedrental.getId());
        sharedInfo.setType(2);
        sharedInfo.setCreateTime(null);
        sharedInfo.setModifyTime(null);
        sharedInfo.setOpenid(sharedrental.getOpenid());

        sharedInfoMapper.insert(sharedInfo);
    }

    @Override
    public Page<Sharedrental> getRental(Page<Sharedrental> page, Sharedrental sharedrental) {
        return sharedrentalMapper.selectPage(page ,null);
    }
}
