package com.wyhnb.main.websocket;

import com.alibaba.fastjson.JSON;
import com.wyhnb.main.entity.Chat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 前后端交互的类实现消息的接收推送(自己发送给另一个人)
 *
 * @ServerEndpoint(value = "/test/oneToOne") 前端通过此URI 和后端交互，建立连接
 */
@Slf4j
@ServerEndpoint(value = "/ws/{openid}", encoders = {ServerEncoder.class})
@Component
public class OneToOneWebSocket {

    /** 记录当前在线连接数 */
    private static AtomicInteger onlineCount = new AtomicInteger(0);

    /** 存放所有在线的客户端 */
    private static Map<String, Session> clients = new ConcurrentHashMap<>();

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session,@PathParam("openid") String openid) {
        onlineCount.incrementAndGet(); // 在线数加1
        clients.put(openid, session);
        log.info("有新连接加入：{}，当前在线人数为：{}", openid, onlineCount.get());
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session,@PathParam("openid") String openid) {
        onlineCount.decrementAndGet(); // 在线数减1
        clients.remove(openid);
        log.info("有一连接关闭：{}，当前在线人数为：{}", openid, onlineCount.get());
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message
     *            客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session,@PathParam("openid") String openid) {
        log.info("服务端收到客户端[{}]的消息[{}]", openid, message);
        try {
            Chat chat = JSON.parseObject(message, Chat.class);
            if (chat != null) {
                Session toSession = clients.get(chat.getToOpenid());
                if (toSession != null) {
                    this.sendMessage(message, toSession,chat.getToOpenid());
                }
            }
        } catch (Exception e) {
            log.error("解析失败：{}", e);
        }
    }

    @OnError
    public void onError(Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    /**
     * 服务端发送消息给客户端
     */
    private void sendMessage(String message, Session toSession,String toOpenid) {
        try {
            log.info("服务端给客户端[{}]发送消息[{}]", toOpenid, message);
            toSession.getBasicRemote().sendText(message);
        } catch (Exception e) {
            log.error("服务端发送消息给客户端失败：{}", e);
        }
    }

}
