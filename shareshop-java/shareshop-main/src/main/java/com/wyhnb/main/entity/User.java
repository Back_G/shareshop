package com.wyhnb.main.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_user")
@ApiModel(value="User对象", description="用户表")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_UUID)
    @ApiModelProperty(value = "用户openid")
    private String openid;

    @ApiModelProperty(value = "用户名")
    private String nickName;

    @ApiModelProperty(value = "头像")
    private String avatarUrl;

    @ApiModelProperty(value = "用户性别")
    private Integer gender;

    @ApiModelProperty(value = "手机")
    private String mobile;

    @ApiModelProperty(value = "用户语言")
    private String language;

    @ApiModelProperty(value = "用户城市")
    private String city;

    @ApiModelProperty(value = "用户省份")
    private String province;

    @ApiModelProperty(value = "用户国家")
    private String country;

    @ApiModelProperty(value = "加入时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;

}
