package com.wyhnb.main.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 共享网络
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_sharedNetwork")
@ApiModel(value="Sharednetwork对象", description="共享网络")
public class Sharednetwork implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ID_WORKER)
    private Long id;

    @ApiModelProperty(value = "发布者openid")
    private String openid;

    @ApiModelProperty(value = "网络共享开始时间")
    private Date sharedTimeStart;

    @ApiModelProperty(value = "网络共享结束时间")
    private Date sharedTimeStop;

    @ApiModelProperty(value = "共享网络价格")
    private Double sharedPrice;

    @ApiModelProperty(value = "联系微信号")
    private String sharedWx;


    @ApiModelProperty(value = "加入时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;


}
