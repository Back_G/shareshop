package com.wyhnb.main.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: Back
 * @Mail: 374616676@qq.com
 * @Description:
 */
@Data
public class FriendsVo {

    @ApiModelProperty(value = "用户openid")
    private String openid;

    @ApiModelProperty(value = "朋友openid")
    private String friendOpenid;

    @ApiModelProperty(value = "用户名")
    private String nickName;

    @ApiModelProperty(value = "头像")
    private String avatarUrl;

    @ApiModelProperty(value = "未读消息")
    private int noRead;

    @ApiModelProperty(value = "最后一条消息，content字段和createTime字段")
    private Object chat;
}
