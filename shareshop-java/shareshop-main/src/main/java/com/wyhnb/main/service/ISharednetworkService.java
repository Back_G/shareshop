package com.wyhnb.main.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyhnb.main.entity.Sharednetwork;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 共享网络 服务类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
public interface ISharednetworkService extends IService<Sharednetwork> {
    Page<Sharednetwork> getNetwork(Page<Sharednetwork> page, Sharednetwork sharednetwork);
}
