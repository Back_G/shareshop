package com.wyhnb.main.mapper;

import com.wyhnb.main.entity.Image;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Repository
public interface ImageMapper extends BaseMapper<Image> {

}
