package com.wyhnb.main.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 丢失物品
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_lostItems")
@ApiModel(value="Lostitems对象", description="丢失物品")
public class Lostitems implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "丢失时间")
    private Date lostTime;

    @ApiModelProperty(value = "物品名称")
    private String itemsName;

    @ApiModelProperty(value = "发布者openid")
    private String openid;

    @ApiModelProperty(value = "联系微信号")
    private String userWx;

    @ApiModelProperty(value = "是否有偿")
    private Integer isItPaid;

    @ApiModelProperty(value = "加入时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;

    @ApiModelProperty(value = "图片列表")
    @TableField(exist = false)
    private List<Image> imgList;
}
