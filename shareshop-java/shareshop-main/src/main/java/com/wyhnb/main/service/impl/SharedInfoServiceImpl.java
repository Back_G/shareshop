package com.wyhnb.main.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wyhnb.main.entity.Idleitems;
import com.wyhnb.main.entity.Image;
import com.wyhnb.main.entity.Lostitems;
import com.wyhnb.main.entity.SharedInfo;
import com.wyhnb.main.mapper.*;
import com.wyhnb.main.service.ISharedInfoService;
import com.wyhnb.main.service.dto.LostitemsDTO;
import com.wyhnb.main.service.dto.SharedInfoPageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SharedInfoServiceImpl extends ServiceImpl<SharedInfoMapper, SharedInfo> implements ISharedInfoService {

    @Autowired
    private SharedInfoMapper sharedInfoMapper;

    @Autowired
    private SharedrentalMapper sharedrentalMapper;

    @Autowired
    private SharednetworkMapper sharednetworkMapper;

    @Autowired
    private LostitemsMapper lostitemsMapper;

    @Autowired
    private ImageMapper imageMapper;

    @Override
    public Page<SharedInfoPageDTO> getPageSharedInfo(Integer page, Integer size) {

        Page<SharedInfoPageDTO> infoPageDTOPage = new Page<>(page, size);

        Page<SharedInfoPageDTO> sharedInfoPageDTOPage= sharedInfoMapper.selectPageAll(infoPageDTOPage);

        for (SharedInfoPageDTO record : sharedInfoPageDTOPage.getRecords()) {
            record.setImgList(imageMapper.selectList(new LambdaQueryWrapper<Image>().eq(Image::getRelationId,record.getId())));
            if(record.getIsLostitems() == 1 ){
                LostitemsDTO lostitems = sharedInfoMapper.getLostItemsBy(record.getId());
                lostitems.setImgList(imageMapper.selectList(new LambdaQueryWrapper<Image>().eq(Image::getRelationType,3).eq(Image::getRelationId,record.getId())));

                record.setLostitems(lostitems);

            }

        }

        return sharedInfoPageDTOPage;
    }
}
