package com.wyhnb.main.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wyhnb.main.entity.Friend;

/**
 * @Author: Back
 * @Mail: 374616676@qq.com
 * @Description:
 */
public interface IFriendService extends IService<Friend> {
}
