package com.wyhnb.main.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wyhnb.main.entity.Image;
import com.wyhnb.main.entity.Lostitems;
import com.wyhnb.main.entity.SharedInfo;
import com.wyhnb.main.mapper.LostitemsMapper;
import com.wyhnb.main.mapper.SharedInfoMapper;
import com.wyhnb.main.service.IImageService;
import com.wyhnb.main.service.ILostitemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;



/**
 * <p>
 * 丢失物品 服务实现类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Service
public class LostitemsServiceImpl extends ServiceImpl<LostitemsMapper, Lostitems> implements ILostitemsService {
    @Autowired
    private LostitemsMapper lostitemsMapper;


    @Autowired
    private IImageService imageService;

    @Autowired
    private SharedInfoMapper sharedInfoMapper;


    @Override
    public void addLostItems(Lostitems lostitems) {
        lostitems.setId(null);
        lostitems.setModifyTime(null);
        lostitems.setCreateTime(null);

        this.save(lostitems);
        List<Image> imgList = new ArrayList<>();
        for (Image image : lostitems.getImgList()) {
            image.setRelationId(lostitems.getId());
            image.setRelationType(3);
            imgList.add(image);
        }
        imageService.saveBatch(imgList);

        SharedInfo sharedInfo = new SharedInfo();
        sharedInfo.setId(lostitems.getId());
        sharedInfo.setType(4);
        sharedInfo.setOpenid(lostitems.getOpenid());
        sharedInfo.setCreateTime(null);
        sharedInfo.setModifyTime(null);
        sharedInfoMapper.insert(sharedInfo);
    }

}
