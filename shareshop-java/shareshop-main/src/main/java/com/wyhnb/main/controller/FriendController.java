package com.wyhnb.main.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wyhnb.main.entity.Friend;
import com.wyhnb.main.mapper.FriendMapper;
import com.wyhnb.main.service.IFriendService;
import com.wyhnb.main.vo.FriendsVo;
import com.wyhnb.main.vo.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Back
 * @Mail: 374616676@qq.com
 * @Description:
 */
@RestController
@Api(tags = "朋友消息表接口")
@RequestMapping("/news")
public class FriendController {

    @Autowired
    private FriendMapper friendMapper;
    @Autowired
    private IFriendService friendService;

    @GetMapping("/getFriends")
    @ApiOperation(value = "获取朋友列表")
    public ResultUtil<List<FriendsVo>> getFriends(@ApiParam(value = "用户openid", name = "openid", required = true) @RequestParam("openid")String openid){
        return ResultUtil.success("success",friendMapper.getFriendsByOpenid(openid));
    }

    @PostMapping("/addFriend")
    @ApiOperation(value = "添加朋友")
    public ResultUtil<?> addFriend(@RequestBody Friend friend){
        friendService.save(friend);
        return ResultUtil.success("执行成功");
    }

    @DeleteMapping("/deleteFriend")
    @ApiOperation(value = "删除好友")
    public ResultUtil<?> deleteFriend(@RequestBody Friend friend){
        QueryWrapper<Friend> wrapper = new QueryWrapper<>();
        wrapper.eq("openid",friend.getOpenid()).eq("friend_openid",friend.getFriendOpenid());
        friendService.remove(wrapper);
        return ResultUtil.success("删除成功！");
    }
}
