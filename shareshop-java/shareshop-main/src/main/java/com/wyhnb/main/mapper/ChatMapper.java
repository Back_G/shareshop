package com.wyhnb.main.mapper;

import com.wyhnb.main.entity.Chat;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
public interface ChatMapper extends BaseMapper<Chat> {

}
