package com.wyhnb.main.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyhnb.main.entity.Idleitems;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 发布闲置物品 Mapper 接口
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */

@Repository
public interface IdleitemsMapper extends BaseMapper<Idleitems> {

    List<Idleitems> getItemsByClassification(@Param("classification")String classification);
}
