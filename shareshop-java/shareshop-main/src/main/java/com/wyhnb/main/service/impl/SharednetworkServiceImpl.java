package com.wyhnb.main.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyhnb.main.entity.Sharednetwork;
import com.wyhnb.main.mapper.SharednetworkMapper;
import com.wyhnb.main.service.ISharednetworkService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 共享网络 服务实现类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Service
public class SharednetworkServiceImpl extends ServiceImpl<SharednetworkMapper, Sharednetwork> implements ISharednetworkService {

    @Autowired
    private SharednetworkMapper sharednetworkMapper;

    @Override
    public Page<Sharednetwork> getNetwork(Page<Sharednetwork> page, Sharednetwork sharednetwork) {
        return sharednetworkMapper.selectPage(page ,null);
    }
}
