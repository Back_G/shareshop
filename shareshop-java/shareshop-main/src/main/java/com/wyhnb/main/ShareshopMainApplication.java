package com.wyhnb.main;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.wyhnb.main.mapper")
public class ShareshopMainApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShareshopMainApplication.class, args);
    }

}
