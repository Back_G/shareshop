package com.wyhnb.main.controller;


import com.wyhnb.main.entity.Chat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import springfox.documentation.annotations.ApiIgnore;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Controller
@ApiIgnore
public class ChatController {

    //@Autowired
    //private SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/ws/{openid}}")
    public void handleMsg(@PathVariable String openid,Object chat){
        System.out.println(openid);
        System.out.println(chat);
    }
}
