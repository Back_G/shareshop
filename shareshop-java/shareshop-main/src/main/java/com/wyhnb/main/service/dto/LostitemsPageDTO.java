package com.wyhnb.main.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class LostitemsPageDTO {
    @ApiModelProperty(value = "丢失时间")
    private Date lostTime;

    @ApiModelProperty(value = "物品名称")
    private String itemsName;

    @ApiModelProperty(value = "是否有偿")
    private Integer isItPaid;
}
