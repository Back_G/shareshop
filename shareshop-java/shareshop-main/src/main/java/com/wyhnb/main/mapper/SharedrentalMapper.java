package com.wyhnb.main.mapper;

import com.wyhnb.main.entity.Sharedrental;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 共享租房 Mapper 接口
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Repository
public interface SharedrentalMapper extends BaseMapper<Sharedrental> {

}
