package com.wyhnb.main.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Back
 * @Mail: 374616676@qq.com
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultUtil<T> implements Serializable {

    private Integer code;
    private String message;
    private T data;


    /**
     * 成功无数据
     *
     * @param msg
     * @return
     */
    public static ResultUtil success(String msg){
        return new ResultUtil(200,msg,null);
    }

    /**
     * 成功有数据
     *
     * @param msg
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResultUtil<T> success(String msg, T data){
        return new ResultUtil<T>(200,msg,data);
    }

    /**
     * 失败提示
     *
     * @param msg
     * @return
     */
    public static ResultUtil error(String msg){
        return new ResultUtil(500,msg,null);
    }

    /**
     * 失败带数据
     *
     * @param msg
     * @param object
     * @return
     */
    public static ResultUtil error(String msg, Object object){
        return new ResultUtil(500,msg,object);
    }

    /**
     * 权限不足,403
     *
     * @param msg
     * @return
     */
    public static ResultUtil forbidden403(String msg){
        return new ResultUtil(403,msg,null);
    }

    /**
     * 权限不足,401
     *
     * @param msg
     * @return
     */
    public static ResultUtil forbidden401(String msg){
        return new ResultUtil(401,msg,null);
    }
}
