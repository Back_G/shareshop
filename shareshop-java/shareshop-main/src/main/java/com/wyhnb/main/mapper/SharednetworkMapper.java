package com.wyhnb.main.mapper;

import com.wyhnb.main.entity.Sharednetwork;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 共享网络 Mapper 接口
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Repository
public interface SharednetworkMapper extends BaseMapper<Sharednetwork> {

}
