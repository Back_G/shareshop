package com.wyhnb.main.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyhnb.main.entity.Image;
import com.wyhnb.main.entity.Lostitems;
import com.wyhnb.main.mapper.ImageMapper;
import com.wyhnb.main.mapper.LostitemsMapper;
import com.wyhnb.main.mapper.SharedInfoMapper;
import com.wyhnb.main.service.ILostitemsService;
import com.wyhnb.main.service.dto.LostitemsDTO;
import com.wyhnb.main.service.dto.LostitemsPageDTO;
import com.wyhnb.main.vo.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 丢失物品 前端控制器
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@RestController
@RequestMapping("/lostitems")
@Api(tags = "丢失物品")
public class LostitemsController {

    @Autowired
    private ILostitemsService lostitemsService;

    @Autowired
    private ImageMapper imageMapper;

    @Autowired
    private LostitemsMapper lostitemsMapper;

    @Autowired
    private SharedInfoMapper sharedInfoMapper;

    @ApiOperation(value = "发布丢失的物品")
    @PostMapping("/addLostItems")
    public ResultUtil<?> addLostItems(@RequestBody Lostitems lostitems) {
        lostitemsService.addLostItems(lostitems);
        return ResultUtil.success("执行成功");
    }

    @ApiOperation(value = "分页查询丢失的物品")
    @PostMapping("/getLostItems")
    public ResultUtil<Page<LostitemsDTO>> addItem(@ApiParam(value = "页码", name = "page", required = true) @RequestParam("page") Integer page,
                                                   @ApiParam(value = "每页数据", name = "size", required = true) @RequestParam("size") Integer size,
                                                   @RequestBody LostitemsPageDTO lostitemsPageDTO) {
        Page<LostitemsDTO> lostitemsPage = new Page<>(page, size);
        lostitemsMapper.getLostItems(lostitemsPage, lostitemsPageDTO);

        for (LostitemsDTO record : lostitemsPage.getRecords()) {
            record.setImgList(imageMapper.selectList(new LambdaQueryWrapper<Image>().eq(Image::getRelationType,3).eq(Image::getRelationId,record.getId())));
        }


        return ResultUtil.success("200", lostitemsPage);
    }

        @ApiOperation(value = "删除查询丢失的物品")
        @DeleteMapping("/deleteItem")
        public ResultUtil<?> deleteItem (@RequestBody Lostitems lostitems){
            lostitemsMapper.deleteById(lostitems.getId());
            sharedInfoMapper.deleteById(lostitems.getId());
            return ResultUtil.success("执行成功");
        }

        @ApiOperation(value = "修改发布信息")
        @PostMapping("/updateItem")
        public ResultUtil<?> updateItem (@RequestBody Lostitems lostitems){
            lostitemsMapper.updateById(lostitems);
            return ResultUtil.success("执行成功");
        }
    }
