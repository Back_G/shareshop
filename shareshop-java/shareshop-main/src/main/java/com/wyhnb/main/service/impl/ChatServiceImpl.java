package com.wyhnb.main.service.impl;

import com.wyhnb.main.entity.Chat;
import com.wyhnb.main.mapper.ChatMapper;
import com.wyhnb.main.service.IChatService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Service
public class ChatServiceImpl extends ServiceImpl<ChatMapper, Chat> implements IChatService {

}
