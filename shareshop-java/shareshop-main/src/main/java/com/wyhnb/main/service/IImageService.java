package com.wyhnb.main.service;

import com.wyhnb.main.entity.Image;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
public interface IImageService extends IService<Image> {

}
