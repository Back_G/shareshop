package com.wyhnb.main.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyhnb.main.entity.Idleitems;
import com.wyhnb.main.entity.Lostitems;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 丢失物品 服务类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
public interface ILostitemsService extends IService<Lostitems> {


    void addLostItems(Lostitems lostitems);

}
