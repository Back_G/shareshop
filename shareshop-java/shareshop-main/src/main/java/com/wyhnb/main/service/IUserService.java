package com.wyhnb.main.service;

import com.wyhnb.main.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
public interface IUserService extends IService<User> {

}
