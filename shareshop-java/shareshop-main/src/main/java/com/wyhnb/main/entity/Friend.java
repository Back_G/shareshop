package com.wyhnb.main.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: Back
 * @Mail: 374616676@qq.com
 * @Description:
 */
@Data
@TableName("t_friend")
@ApiModel(value="Friend对象")
@Accessors(chain = true)
public class Friend implements Serializable {

    @TableId(type = IdType.AUTO)
    private int id;

    @ApiModelProperty(value = "openid")
    private String openid;

    @ApiModelProperty(value = "朋友openid")
    private String friendOpenid;

    @ApiModelProperty(value = "未读量")
    private int noRead;

    @ApiModelProperty(value = "最后一条消息，content字段和createTime字段")
    private Object chat;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

}
