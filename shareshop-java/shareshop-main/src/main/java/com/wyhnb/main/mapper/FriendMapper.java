package com.wyhnb.main.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyhnb.main.entity.Friend;
import com.wyhnb.main.vo.FriendsVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: Back
 * @Mail: 374616676@qq.com
 * @Description:
 */
@Repository
public interface FriendMapper extends BaseMapper<Friend> {

    List<FriendsVo> getFriendsByOpenid(@Param("openid")String openid);
}
