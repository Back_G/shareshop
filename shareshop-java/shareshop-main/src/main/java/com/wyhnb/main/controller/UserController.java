package com.wyhnb.main.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wyhnb.main.entity.User;
import com.wyhnb.main.mapper.UserMapper;
import com.wyhnb.main.vo.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@RestController
@RequestMapping("/user")
@Api(tags = "用户")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @PostMapping("addUser")
    @ApiOperation("添加用户")
    public ResultUtil<Integer> addUser(@RequestBody User user) {
        if (userMapper.selectCount(new LambdaQueryWrapper<User>().eq(User::getOpenid, user.getOpenid())) == 0) {
            return ResultUtil.success("200", userMapper.insert(user));
        } else {
            return ResultUtil.success("200", userMapper.updateById(user));
        }

    }

    @GetMapping("getUser")
    @ApiOperation("查找用户")
    public ResultUtil<User> getUser(@RequestParam("openid") String openid) {
        return ResultUtil.success("200", userMapper.selectById(openid));
    }

    @PutMapping("updateUser")
    @ApiOperation("修改用户")
    public ResultUtil<Integer> updateUser(@RequestBody User user) {
        return ResultUtil.success("200", userMapper.updateById(user));
    }

    @DeleteMapping("DeleteUser")
    @ApiOperation("删除用户")
    public ResultUtil<Integer> DeleteUser(@RequestParam("openid") String openid) {
        return ResultUtil.success("200", userMapper.deleteById(openid));
    }
}
