package com.wyhnb.main.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyhnb.main.entity.*;
import com.wyhnb.main.mapper.*;
import com.wyhnb.main.service.IIdleitemsService;
import com.wyhnb.main.service.ISharedInfoService;
import com.wyhnb.main.service.ISharednetworkService;
import com.wyhnb.main.service.ISharedrentalService;
import com.wyhnb.main.vo.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 共享租房 前端控制器
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@RestController
@RequestMapping("/shared")
@Api(tags = "共享物品")
public class ShareController {

    @Autowired
    private IIdleitemsService idleitemsService;
    @Autowired
    private IdleitemsMapper idleitemsMapper;
    @Autowired
    private ImageMapper imageMapper;

    @Autowired
    private ISharednetworkService sharednetworkService;
    @Autowired
    private SharednetworkMapper sharednetworkMapper;

    @Autowired
    private ISharedrentalService sharedrentalService;

    @Autowired
    private SharedrentalMapper sharedrentalMapper;

    @Autowired
    private SharedInfoMapper sharedInfoMapper;

    @Autowired
    private ISharedInfoService sharedInfoService;

    @Autowired
    private LostitemsMapper lostitemsMapper;

    @ApiOperation(value = "共享物品、闲置物品")
    @PostMapping("/addItem")
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<?> addItem(@RequestBody Idleitems idleitems) {
        idleitemsService.addItem(idleitems);
        return ResultUtil.success("执行成功");
    }

    @ApiOperation(value = "分页获取共享物品、闲置物品")
    @GetMapping("/getItem")
    public ResultUtil<Page<Idleitems>> getItem(@ApiParam(value = "页码", name = "page", required = true) @RequestParam("page") Integer page,
                                               @ApiParam(value = "每页数据", name = "size", required = true) @RequestParam("size") Integer size,
                                               Idleitems idleitems) {

        return ResultUtil.success("执行成功", idleitemsService.getItem(new Page<Idleitems>(page, size), idleitems));
    }

    @ApiOperation(value = "删除共享物品、闲置物品信息")
    @DeleteMapping("/deleteItem")
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<?> deleteItem(@RequestBody Idleitems idleitems) {
        idleitemsMapper.deleteById(idleitems.getId());
        sharedInfoMapper.deleteById(idleitems.getId());
        return ResultUtil.success("执行成功");
    }

    @ApiOperation(value = "修改共享物品、闲置物品信息")
    @PostMapping("/updateItem")
    public ResultUtil<?> updateItem(@RequestBody Idleitems idleitems) {
        idleitemsMapper.updateById(idleitems);
        return ResultUtil.success("执行成功");
    }


    @ApiOperation(value = "共享网络")
    @PostMapping("/addNetwork")
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<?> addNetwork(@RequestBody Sharednetwork sharednetwork) {
        sharednetwork.setId(null);
        sharednetworkService.save(sharednetwork);

        SharedInfo sharedInfo = new SharedInfo();
        sharedInfo.setId(sharednetwork.getId());
        sharedInfo.setType(3);
        sharedInfo.setOpenid(sharednetwork.getOpenid());
        sharedInfo.setCreateTime(null);
        sharedInfo.setModifyTime(null);
        sharedInfoMapper.insert(sharedInfo);

        return ResultUtil.success("执行成功");
    }

    @ApiOperation(value = "分页获取共享网络")
    @GetMapping("/getNetwork")
    public ResultUtil<Page<Sharednetwork>> getNetwork(@ApiParam(value = "页码", name = "page", required = true) @RequestParam("page") Integer page,
                                                      @ApiParam(value = "每页数据", name = "size", required = true) @RequestParam("size") Integer size,
                                                      Sharednetwork sharednetwork) {

        return ResultUtil.success("执行成功", sharednetworkService.getNetwork(new Page<Sharednetwork>(page, size), sharednetwork));
    }

    @ApiOperation(value = "删除共享网络的信息")
    @DeleteMapping("/deleteNetwork")
    public ResultUtil<?> deleteNetwork(@RequestBody Sharednetwork sharednetwork) {
        sharednetworkMapper.deleteById(sharednetwork.getId());
        sharedInfoMapper.deleteById(sharednetwork.getId());
        return ResultUtil.success("执行成功");
    }

    @ApiOperation(value = "修改共享网络的信息")
    @PostMapping("/updateNetwork")
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<?> updateNetwork(@RequestBody Sharednetwork sharednetwork) {
        sharednetworkMapper.updateById(sharednetwork);
        return ResultUtil.success("执行成功");
    }


    @ApiOperation(value = "共享租房")
    @PostMapping("/addRental")
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<?> addRental(@RequestBody Sharedrental sharedrental) {
        //System.out.println(sharedrental);
        sharedrentalService.addRental(sharedrental);
        return ResultUtil.success("执行成功");
    }

    @ApiOperation(value = "获取共享租房")
    @GetMapping("/getRental")
    public ResultUtil<Page<Sharedrental>> getRental(@ApiParam(value = "页码", name = "page", required = true) @RequestParam("page") Integer page,
                                                    @ApiParam(value = "每页数据", name = "size", required = true) @RequestParam("size") Integer size,
                                                    Sharedrental sharedrental) {

        return ResultUtil.success("执行成功", sharedrentalService.getRental(new Page<Sharedrental>(page, size), sharedrental));
    }

    @ApiOperation(value = "删除共享租房信息")
    @DeleteMapping("/deleteRental")
    public ResultUtil<?> deleteRental(@RequestBody Sharedrental sharedrental) {
        sharedrentalMapper.deleteById(sharedrental.getId());
        sharedInfoMapper.deleteById(sharedrental.getId());
        return ResultUtil.success("执行成功");
    }

    @ApiOperation(value = "修改共享租房信息")
    @PostMapping("/updateRental")
    public ResultUtil<?> updateRental(@RequestBody Sharedrental sharedrental) {
        sharedrentalMapper.updateById(sharedrental);
        return ResultUtil.success("执行成功");
    }

    @ApiOperation(value = "分页查询分享三合一信息")
    @GetMapping("/getPageSharedInfo")
    public ResultUtil<?> getPageSharedInfo(@ApiParam(value = "页码", name = "page", required = true) @RequestParam("page") Integer page,
                                           @ApiParam(value = "每页数据", name = "size", required = true) @RequestParam("size") Integer size) {


        return ResultUtil.success("200", sharedInfoService.getPageSharedInfo(page, size));
    }

    @ApiOperation(value = "查询详情")
    @GetMapping("/getSharedInfo/{type}/{id}")
    public ResultUtil<?> getPageSharedInfo(@PathVariable("type") Integer type,@PathVariable("id") Long id) {
        if(type == 1){
            Idleitems idleitems = idleitemsMapper.selectById(id);
            idleitems.setImgList(imageMapper.selectList(new LambdaQueryWrapper<Image>()
                    .eq(Image::getRelationId,id)));

            return ResultUtil.success("200", idleitems);

        }else if(type == 2){
            Sharedrental sharedrental = sharedrentalMapper.selectById(id);
            sharedrental.setImgList(imageMapper.selectList(new LambdaQueryWrapper<Image>()
                    .eq(Image::getRelationId,id)));

            return ResultUtil.success("200", sharedrental);
        }else if(type == 3){
            Sharednetwork sharednetwork = sharednetworkMapper.selectById(id);

            return ResultUtil.success("200", sharednetwork);
        }else if(type == 4){
            Lostitems lostitems = lostitemsMapper.selectById(id);
            lostitems.setImgList(imageMapper.selectList(new LambdaQueryWrapper<Image>()
                    .eq(Image::getRelationId,id)));

            return ResultUtil.success("200", lostitems);
        }else {
            return ResultUtil.error("找不到对应类别" );
        }

    }

    @ApiOperation(value = "获取共享闲置物品列表by类别")
    @GetMapping("/getItemsByClassification")
    public ResultUtil<List<Idleitems>> getItemsByClassification(
            @ApiParam(value = "分类名字", name = "classification", required = true) @RequestParam("classification") String classification){


        return ResultUtil.success("success",idleitemsMapper.getItemsByClassification(classification));
    }

}
