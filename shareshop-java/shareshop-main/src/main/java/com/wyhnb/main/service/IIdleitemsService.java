package com.wyhnb.main.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyhnb.main.entity.Idleitems;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wyhnb.main.entity.Sharednetwork;
import com.wyhnb.main.entity.Idleitems;

/**
 * <p>
 * 发布闲置物品 服务类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
public interface IIdleitemsService extends IService<Idleitems> {

    void addItem(Idleitems idleitems);

    Page<Idleitems> getItem(Page<Idleitems> page,Idleitems idleitems);


}
