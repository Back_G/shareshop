package com.wyhnb.main.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wyhnb.main.entity.SharedInfo;
import com.wyhnb.main.service.dto.SharedInfoPageDTO;

public interface ISharedInfoService extends IService<SharedInfo> {

    Page<SharedInfoPageDTO> getPageSharedInfo(Integer page,Integer size);
}
