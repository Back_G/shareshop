package com.wyhnb.main.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyhnb.main.entity.Sharednetwork;
import com.wyhnb.main.entity.Sharedrental;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 共享租房 服务类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
public interface ISharedrentalService extends IService<Sharedrental> {

    void addRental(Sharedrental sharedrental);

    Page<Sharedrental> getRental(Page<Sharedrental> page, Sharedrental sharedrental);

}
