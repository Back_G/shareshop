package com.wyhnb.main.service.impl;

import com.wyhnb.main.entity.Image;
import com.wyhnb.main.mapper.ImageMapper;
import com.wyhnb.main.service.IImageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Service
public class ImageServiceImpl extends ServiceImpl<ImageMapper, Image> implements IImageService {

}
