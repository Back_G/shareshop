package com.wyhnb.main.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 共享、闲置物品
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_idleItems")
@ApiModel(value = "Idleitems对象", description = "共享、闲置物品")
public class Idleitems implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ID_WORKER)
    private Long id;

    @ApiModelProperty(value = "发布用户openid")
    private String openid;

    @ApiModelProperty(value = "物品标题")
    private String itemTitle;

    @ApiModelProperty(value = "物品描述")
    private String itemDesc;

    @ApiModelProperty(value = "物品分类")
    private String itemClassification;

    @ApiModelProperty(value = "是否在线交易")
    private boolean sellOnline;

    @ApiModelProperty(value = "是否共享物品")
    private int isShareItem;

    @ApiModelProperty(value = "物品价格")
    private Double price;

    @ApiModelProperty(value = "联系微信号")
    private String userWx;

    @ApiModelProperty(value = "加入时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date modifyTime;

    @ApiModelProperty(value = "图片列表")
    @TableField(exist = false)
    private List<Image> imgList;

}
