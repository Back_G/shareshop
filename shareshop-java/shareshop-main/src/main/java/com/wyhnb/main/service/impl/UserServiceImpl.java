package com.wyhnb.main.service.impl;

import com.wyhnb.main.entity.User;
import com.wyhnb.main.mapper.UserMapper;
import com.wyhnb.main.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
