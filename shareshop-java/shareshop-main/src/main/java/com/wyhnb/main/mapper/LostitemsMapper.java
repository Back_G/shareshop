package com.wyhnb.main.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyhnb.main.entity.Lostitems;
import com.wyhnb.main.service.dto.LostitemsDTO;
import com.wyhnb.main.service.dto.LostitemsPageDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 * 丢失物品 Mapper 接口
 * </p>
 *
 * @author back
 * @since 2021-05-27
 */
@Repository
@Mapper
public interface LostitemsMapper extends BaseMapper<Lostitems> {

    Page<LostitemsDTO> getLostItems(@Param("page") Page<LostitemsDTO> page, @Param("lostitemsPageDTO") LostitemsPageDTO lostitemsPageDTO );
}
