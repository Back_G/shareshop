// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'jiayu-3gisznms32bc4e55',
})

const db = cloud.database()
const _ = db.command
// 云函数入口函数
exports.main = async (event, context) => {
  return await db.collection('community').add({
    data:{
      time:event.time,
      content:event.content,
      openid:event.openid,
      likePeople:[],
      alreadyread:[]
    }
  })
}