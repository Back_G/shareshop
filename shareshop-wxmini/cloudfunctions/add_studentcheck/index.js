// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'jiayu-3gisznms32bc4e55',
})

const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  return await db.collection('studentstatus').add({
    data: {
      name: event.name,
      studentid: event.studentid,
      grade: event.grade,
      major:event.major,
      openid: event.openid,
      imgList: event.img,
      statue:"1",
      time:event.time,
      remark:'无'
    }
  })
}