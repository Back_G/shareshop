// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'jiayu-3gisznms32bc4e55',
})

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()

  return {
    event,
    openid: wxContext.OPENID,
  }
}