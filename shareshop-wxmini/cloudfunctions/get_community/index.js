// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'jiayu-3gisznms32bc4e55',
})
var $ = cloud.database().command.aggregate
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  return await db.collection('community').aggregate()
  .lookup({
    from:"userinfo", 
    localField: 'openid', 
    foreignField: 'openid', 
    as: 'newcommunity' 
  })
  .replaceRoot({  
    newRoot: $.mergeObjects([$.arrayElemAt(['$newcommunity', 0]), '$$ROOT'])
  })
  .project({
    newcommunity: 0
  })
  .end({
    success:function(res){
      return res;
    },
    fail(error) {
      return error;
    }
  })
}