// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'jiayu-3gisznms32bc4e55',
})
const db = cloud.database()


// 云函数入口函数
exports.main = async (event, context) => {
  try {
    return await db.collection('userinfo')
    .where({
      openid: event.openid
    })
      .get({
        success: function (res) {
          return res
        }
      })
  } catch (e) {
    console.error(e);
  }
}