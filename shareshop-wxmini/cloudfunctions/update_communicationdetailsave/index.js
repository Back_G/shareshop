// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'jiayu-3gisznms32bc4e55',
})
const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  try {
    var _id = event._id
    var saveList = event.saveList
    return await db.collection('communitysave')
      .doc(_id)
      .update({
        data:{
          saveList:saveList,
        },
        success: function (res) {
          return res
        }
      })
  } catch (e) {
    console.error(e);
  }
}