// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'jiayu-3gisznms32bc4e55',
})
const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  try {
    var openid = event.openid
    var value = event.userinfo
    return await db.collection('userinfo')
      .doc(openid)
      .update({
        data:{
          userinfo:value
        },
        success: function (res) {
          return res
        }
      })
  } catch (e) {
    console.error(e);
  }
}