const app = getApp()
let leftHeight = 0
let rightHeight = 0

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    list: {
      type: Object,
      value: [],
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    leftList: [],
    rightList: [],
  },

  /**
   * 组件的方法列表
   */
  
  methods: {
    bindload(e) {
      var that = this;
      var windowWidth = wx.getSystemInfoSync().windowWidth;
      var index = e.currentTarget.dataset.index;
      let imgHeight = windowWidth * 0.48 / e.detail.width * e.detail.height;
      let item = that.data.list[index]
      item.height = imgHeight

      let textHeight = item.name && item.name.length > 9 ? 80 : 60 // 文字高度
      let heightDiv = imgHeight + textHeight // 外盒子高度:图片+图片下面工具栏高度

      let loadingCount = this.data.loadingCount - 1;
      let leftList = this.data.leftList;
      let rightList = this.data.rightList;

      if (leftHeight <= rightHeight) {
        leftHeight += heightDiv;
        leftList.push(item);
      } else {
        rightHeight += heightDiv;
        rightList.push(item);
      }

      let data = {
        loadingCount: loadingCount,
        leftList: leftList,
        rightList: rightList
      };

      if (!loadingCount) {
        data.images = [];
      }
      this.setData(data);
    },
    toDetals(e){
      let id = e.currentTarget.dataset.id
      let type = e.currentTarget.dataset.type
      wx.navigateTo({
        url: '../../pages/shopdetail/shopdetail?id='+id+'&&type='+type,
      })
    }

  }
})