var util = require('../../../utils/util');
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    picker: ['2016', '2017', '2018', '2019', '2020', '2021', '2022'],
    imgList: [],
  },
  onShow: function () {
    wx.showToast({
      title: 'loading',
      icon: 'loading',
    })
    wx.cloud.callFunction({
      name: "get_studentcheck",
      data: {
        openid:app.globalData.openid 
      },
    }).then(res => {
      var examinemessage = res.result.data[0]
      if (res.result.data.length == 0) {
        this.setData({
          examine: 'none',
        })
      } else if (res.result.data[0].statue == "1") {
        this.setData({
          examine: 'wait',
          examinemessage: examinemessage
        })
      } else if (res.result.data[0].statue == "2") {
        this.setData({
          examine: 'fail',
          examinemessage: examinemessage
        })
      } else if (res.result.data[0].statue == "3") {
        this.setData({
          examine: 'success',
          examinemessage: examinemessage
        })
      }
    })
  },
  PickerChange(e) {
    this.setData({
      index: e.detail.value
    })
  },
  // notification: function (e){
  //     wx.requestSubscribeMessage({
  //       tmplIds: ['PNlo0zPiWo2Cq6hIiU77MwDOoroDnAZ4HNSEVaqN8rs'],
  //       success: res => {
  //         console.log(res)
  //       },
  //       fail: err => {
  //         console.log('err', err)
  //       }
  //     })
  // },
  formBindsubmit: function (e) {
    //获取时间戳
    this.time = new Date();
    //时间戳转换
    var time = util.formatTime(this.time);
    var that = this
    var imgList = that.data.imgList;
    var name = e.detail.value.name
    var studentid = e.detail.value.studentid  //学号
    var grade = that.data.picker[e.detail.value.grade] //获取年级
    var major = e.detail.value.major
    wx.showLoading({
      title: '发布中',
    })

    if (name == '') {
      wx.showToast({
        title: '请输入姓名',
        icon: 'none',
        duration: 2000,
      })
      return false
    } else if (studentid == '') {
      wx.showToast({
        title: '请输入学号',
        icon: 'none',
        duration: 2000,
      })
      return false
    } else if (studentid.length != 10) {
      wx.showToast({
        title: '输入学号有误',
        icon: 'none',
        duration: 2000,
      })
      return false
    } else if (grade == '') {
      wx.showToast({
        title: '请输入年级',
        icon: 'none',
        duration: 2000,
      })
      return false
    } else if (major == '') {
      wx.showToast({
        title: '请输入专业',
        icon: 'none',
        duration: 2000,
      })
      return false
    }

    var imgname = imgList[0].split("/").pop();

    wx.cloud.uploadFile({
      cloudPath: "examine/" + imgname,
      filePath: imgList[0],
      success: res => {
        wx.cloud.callFunction({
          name: 'add_studentcheck',
          data: {
            openid:app.globalData.openid,
            name: name,
            studentid: studentid,
            grade: grade,
            major: major,
            img: res.fileID,
            time:time
          },
          success(res) {
            wx.showToast({
              title: '成功上传',
              icon: 'success',
              duration: 1000,
              mask: true,
              success: function () {
                setTimeout(function () {
                  //要延时执行的代码
                  wx.switchTab({
                    url: '../mine/mine',
                  })
                }, 1000) //延迟时间
              },
            });
          },
          fail(res) {
            console.log(res)
          }
        })
      }
    })


  },
  ChooseImage() {
    wx.chooseImage({
      count: 1, //默认9
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], //从相册选择
      success: (res) => {
        if (this.data.imgList.length != 0) {
          this.setData({
            imgList: this.data.imgList.concat(res.tempFilePaths)
          })
        } else {
          this.setData({
            imgList: res.tempFilePaths
          })
        }
      }
    });
  },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '删除',
      content: '确定要删除这张照片？',
      cancelText: '再看看',
      confirmText: '删除',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList
          })
        }
      }
    })
  },
  cancel() {
    wx.showModal({
      title: '取消申请',
      content: '确定要取消申请嘛？',
      success: res => {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: "delet_studentcheck",
            data: {
              openid: app.globalData.openid
            },
          }).then(res => {
            wx.showToast({
              title: '申请已取消',
              icon: 'success',
              duration: 1000,
              mask: true,
              success: function () {
                setTimeout(function () {
                  //要延时执行的代码
                  wx.navigateBack({
                    delta: 1
                  })
                }, 1000) //延迟时间
              },
            })
          })
        }
      }
    })
  },
})