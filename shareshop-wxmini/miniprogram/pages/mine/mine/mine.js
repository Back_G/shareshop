const app = getApp()
import {client} from "../../../utils/request"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    userInfo: {
      avatarUrl: "../../../images/empower.png",
      nickName: "点击登录",
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(app.globalData.openid)
  },
  onShow: function () {
    var userinfo = wx.getStorageSync('userInfo')
    if(userinfo){
      this.setData({
        userInfo: userinfo
      });
    }
    this.tabBar();
  },
   getUserProfile(e) {
    wx.getUserProfile({
      desc: '用于用户资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        wx.setStorageSync('userInfo', res.userInfo)
        let newuserinfo = res.userInfo
        newuserinfo.openid = app.globalData.openid
          client({
            url:'user/addUser',
            data:newuserinfo,
            method:'post'
          })

        wx.cloud.callFunction({
          name:"get_userinfo",
          data:{openid:app.globalData.openid}
        }).then(res=>{
          var userinfo = wx.getStorageSync('userInfo',res.userInfo)
          if(res.result.data.length == 0){
            wx.cloud.callFunction({
              name:"add_userinfo",
              data:{
                userinfo:userinfo,
                openid:app.globalData.openid
              }
            })
          }else{
            wx.cloud.callFunction({
              name:"update_userinfo",
              data:{
                userinfo:userinfo,
                openid:app.globalData.openid
              }
            })
          }
        })

        this.setData({
          userInfo: res.userInfo
        });
      }
    })

  },
  tabBar(){
    if(typeof this.getTabBar === 'function' && this.getTabBar()){
      this.getTabBar().setData({
        selected:4
      })
    }
  },

  toshowminemessage(){
    wx.navigateTo({
      url: '../usercheck/index',
    })
  }
})