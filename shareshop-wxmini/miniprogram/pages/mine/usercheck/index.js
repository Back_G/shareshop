const app = getApp()
var that = this
Page({
  data: {
  },
  onLoad: function (options) {

  },
  onShow() {
    this.getstudentcheck()
  },
  async getstudentcheck() {
    const res = await wx.cloud.callFunction({
      name: "get_studentcheck",
      data: {
        openid: app.globalData.openid
      }
    })
    if (res.result.data.length == 0) {
      this.getteachercheck()
    } else {
      this.setData({
        studentdata:true,
        shows:false
      })
      var examinemessage = res.result.data[0]
      if (res.result.data[0].statue == "1") {
        this.setData({
          examine: 'wait',
          examinemessage: examinemessage
        })
      } else if (res.result.data[0].statue == "2") {
        this.setData({
          examine: 'fail',
          examinemessage: examinemessage
        })
      } else if (res.result.data[0].statue == "3") {
        this.setData({
          examine: 'success',
          examinemessage: examinemessage
        })
      }
      return false
    }
  },
  async getteachercheck() {
    const res = await wx.cloud.callFunction({
      name: "get_teachercheck",
      data: {
        openid: app.globalData.openid
      }
    })
    if (res.result.data.length == 0) {
      this.outpeoplecheck()
    } else {
      this.setData({
        teacherdata:true,
        shows:false
      })
      var examinemessage = res.result.data[0]
      if (res.result.data[0].statue == "1") {
        this.setData({
          examine: 'wait',
          examinemessage: examinemessage
        })
      } else if (res.result.data[0].statue == "2") {
        this.setData({
          examine: 'fail',
          examinemessage: examinemessage
        })
      } else if (res.result.data[0].statue == "3") {
        this.setData({
          examine: 'success',
          examinemessage: examinemessage
        })
      }
      return false
    }
  },
  async outpeoplecheck() {
    const res = await wx.cloud.callFunction({
      name: "get_outpeoplecheck",
      data: {
        openid: app.globalData.openid
      }
    })
    if (res.result.data.length == 0) {
      return true
    } else {
      this.setData({
        outpeopledata:true,
        shows:false
      })
      var examinemessage = res.result.data[0]
      if (res.result.data[0].statue == "1") {
        this.setData({
          examine: 'wait',
          examinemessage: examinemessage
        })
      } else if (res.result.data[0].statue == "2") {
        this.setData({
          examine: 'fail',
          examinemessage: examinemessage
        })
      } else if (res.result.data[0].statue == "3") {
        this.setData({
          examine: 'success',
          examinemessage: examinemessage
        })
      }
      return false
    }
  },

  studentcancel() {
    wx.showModal({
      title: '取消申请',
      content: '确定要取消申请嘛？',
      success: res => {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: "delet_studentcheck",
            data: {
              openid: app.globalData.openid
            },
          }).then(res => {
            wx.showToast({
              title: '申请已取消',
              icon: 'success',
              duration: 1000,
              mask: true,
              success: function () {
                setTimeout(function () {
                  //要延时执行的代码
                  wx.navigateBack({
                    delta: 1
                  })
                }, 1000) //延迟时间
              },
            })
          })
        }
      }
    })
  },
  teachercancel() {
    wx.showModal({
      title: '取消申请',
      content: '确定要取消申请嘛？',
      success: res => {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: "delet_teachercheck",
            data: {
              openid: app.globalData.openid
            },
          }).then(res => {
            wx.showToast({
              title: '申请已取消',
              icon: 'success',
              duration: 1000,
              mask: true,
              success: function () {
                setTimeout(function () {
                  //要延时执行的代码
                  wx.navigateBack({
                    delta: 1
                  })
                }, 1000) //延迟时间
              },
            })
          })
        }
      }
    })
  },



  canceloutpeople() {
    wx.showModal({
      title: '取消申请',
      content: '确定要取消申请嘛？',
      success: res => {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: "delet_outpeoplecheck",
            data: {
              openid: app.globalData.openid
            },
          }).then(res => {
            wx.showToast({
              title: '申请已取消',
              icon: 'success',
              duration: 1000,
              mask: true,
              success: function () {
                setTimeout(function () {
                  //要延时执行的代码
                  wx.navigateBack({
                    delta: 1
                  })
                }, 1000) //延迟时间
              },
            })
          })
        }
      }
    })
  }
})