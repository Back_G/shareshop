const app = getApp()
var utils = require('../../../utils/webutil.js');
var websocket = require('../../../utils/websocket.js');
var avatar = '' // 本用户头像
var toOpenid = '' // 需要发信息给哪个用户openid
var openid  // 本用户openid

Page({
  data: {
    inputValue:'',
    chatList:[]
  },
  onLoad(option){
    toOpenid = option.toOpenid
    let nickName = option.nickName
    wx.setNavigationBarTitle({
      title: nickName,
    })
    var userinfo = wx.getStorageSync('userInfo')
    if(userinfo){
      avatar=userinfo.avatarUrl
    }
    openid = app.globalData.openid
    //监听信息
    wx.onSocketMessage((result) => {
      let msg = JSON.parse(result.data)

      //监听每条信息，改变全局变量friendList的值
      let formatmsg = this.formatMsg(msg)
      let friendList = app.globalData.friendList
      console.log(msg)
      console.log(friendList)
      let index = friendList.findIndex(item=>item.friendOpenid === msg.fromOpenid)
      friendList[index].chat = formatmsg
      app.globalData.friendList = friendList

      // 将监听到的信息存入本地聊天记录（缓存）
      msg.self=false
      let chatList = wx.getStorageSync(msg.fromOpenid);
      if(chatList){
        chatList.push(msg)
      }else{
        chatList = [msg]
      }
      this.setData({chatList:chatList})
       //拉到最底部
      wx.pageScrollTo({
        scrollTop: 99999999999999999
      });
      wx.setStorageSync(toOpenid, chatList)
    })
  },
  onShow(){
    let chatList = wx.getStorageSync(toOpenid);
    this.setData({chatList:chatList})
    wx.pageScrollTo({
      scrollTop: 99999999999999999
    });
  },
  InputBlur(e) {
    this.setData({
      inputValue: e.detail.value
    })
  },
  send(){
    if(this.data.inputValue === null||this.data.inputValue===''){
      return
    }
    let param = {
      self:true,
      avatar:avatar,
      content:this.data.inputValue,
      createTime:utils.formatTime(new Date())
    }
    let chatList = this.data.chatList
    if(chatList){
      chatList.push(param)
    }else{
      chatList = [param]
    }

    this.setData({
      chatList:chatList,
      inputValue: ''
    })

    // 发送信息改变friendList里的chat值
    let friendList = app.globalData.friendList
    let index = friendList.findIndex(item=>item.friendOpenid === toOpenid)
    let chat = this.formatMsg(param)
    friendList[index].chat = chat
    app.globalData.friendList = friendList

    //拉到最底部
    wx.pageScrollTo({
      scrollTop: 99999999999999999
    });
    param.fromOpenid=openid
    param.toOpenid = toOpenid

    let msgList =  wx.getStorageSync(param.toOpenid);
    if(msgList){
      msgList.push(param)
      wx.setStorageSync(param.toOpenid, msgList)
    }else{
      wx.setStorageSync(param.toOpenid, [param])
    }
    websocket.send(JSON.stringify(param))
  },

  // 格式化监听到的值，方便前端渲染
  formatMsg(msg){
    let createTime = msg.createTime.split(' ')[1]
    return {
      "content":msg.content,
      "createTime":createTime
    }
  },

})