const app = getApp()
const {client}  = require("../../../utils/request")
Page({
  data:{
    friendList:[]
  },

  onShow(){
    this.getFriendList()
     //监听信息
     wx.onSocketMessage((result) => {
      let msg = JSON.parse(result.data)
      let formatmsg = this.formatMsg(msg)
      let friendList = app.globalData.friendList
      let index = friendList.findIndex(item=>item.friendOpenid === msg.fromOpenid)
      let noReadStr = 'friendList['+index+'].noRead'
      let chatStr = 'friendList['+index+'].chat'

      this.setData({
        [noReadStr]:friendList[index].noRead+1,
        [chatStr]:formatmsg
      })

      // 讲目前friendList状态存到全局变量
      app.globalData.friendList = this.data.friendList

      // 将监听到的信息存入本地聊天记录（缓存）
      msg.self=false
      let chatList = wx.getStorageSync(msg.fromOpenid);
      if(chatList){
        chatList.push(msg)
      }else{
        chatList = [msg]
      }
      wx.setStorageSync(msg.fromOpenid, chatList)
  })
  this.tabBar()
  },

   // 获取朋友信息
   getFriendList(){
    let openid = app.globalData.openid
    // 全局变量中friendList为空，就请求朋友列表
    if(app.globalData.friendList == null){
      wx.showToast({
        title: '加载中...',
        icon:'loading',
        duration:2000,
        mask:true
      })
      client({
        url:'news/getFriends?openid='+openid,
        method:'GET'
      }).then(res=>{
        this.setData({
          friendList:res.data.data
        })
        app.globalData.friendList = res.data.data

      })
     
    }else{
      this.setData({
        friendList:app.globalData.friendList
      })
    }
  },

  // 格式化监听到的值，方便前端渲染
  formatMsg(msg){
    let createTime = msg.createTime.split(' ')[1]
    return {
      "content":msg.content,
      "createTime":createTime
    }
  },

  toChat(e){
    let index = e.currentTarget.dataset.index
    let toOpenid = e.currentTarget.dataset.openid
    let nickName = e.currentTarget.dataset.nickname

    let friendList = app.globalData.friendList
    friendList[index].noRead = 0
    app.globalData.friendList = friendList
    wx.navigateTo({
      url: '../chat/chat?toOpenid='+toOpenid+'&&nickName='+nickName
    })
  },
  readTap(e){
    let index = e.currentTarget.dataset.index

    let friendList = app.globalData.friendList
    friendList[index].noRead = 0
    app.globalData.friendList = friendList

    let noReadStr = 'friendList['+index+'].noRead'
    this.setData({
      [noReadStr]:0
    })
  },
  deleteTap(e){
    let index = e.currentTarget.dataset.index
    let friendList = this.data.friendList
    let that = this
    client({
      url:'news/deleteFriend',
      data:friendList[index],
      method:'DELETE'
    })
    friendList.splice(index, 1)
    // 延迟动画，提高操作流畅性
    setTimeout(function(){
      that.setData({
        friendList:friendList
      })
    },500)
    app.globalData.friendList = friendList
  },
  // ListTouch触摸开始
  ListTouchStart(e) {
    this.setData({
      ListTouchStart: e.touches[0].pageX
    })
  },
  // ListTouch计算方向
  ListTouchMove(e) {
    this.setData({
      ListTouchDirection: e.touches[0].pageX - this.data.ListTouchStart > 0 ? 'right' : 'left'
    })
  },

  // ListTouch计算滚动
  ListTouchEnd(e) {
    if (this.data.ListTouchDirection =='left'){
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    } else {
      this.setData({
        modalName: null
      })
    }
    this.setData({
      ListTouchDirection: null
    })
  },
  tabBar(){
    if(typeof this.getTabBar === 'function' && this.getTabBar()){
      this.getTabBar().setData({
        selected:3
      })
    }
  },
})