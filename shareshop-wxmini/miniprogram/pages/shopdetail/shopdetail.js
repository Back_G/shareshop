const app = getApp()
const {client}  = require("../../utils/request")
const webutil  = require("../../utils/webutil")


Page({

  data: {
    userInfo:{},
    shopDetiails:{},
    type:null,
  },


  onLoad: function (options) {
    let id = options.id
    let type = options.type
    // 获取shopDetails
    client({
      url:'shared/getSharedInfo/'+type+'/'+id,
      method:'GET'
    }).then(res=>{ 
      let shopDetails = res.data.data
      shopDetails.createTime = webutil.formatTime8(new Date(Date.parse(shopDetails.createTime)))

      if(type=='3'){ // 共享网络处理
        shopDetails.sharedTimeStart= webutil.formatDate(new Date(Date.parse(shopDetails.sharedTimeStart)))
        shopDetails.sharedTimeStop= webutil.formatDate(new Date(Date.parse(shopDetails.sharedTimeStop)))
      }else if(type=='2'){ // 共享租房处理
        shopDetails.sharedTimeStart = webutil.formatDate(new Date(Date.parse(shopDetails.sharedTimeStart)))
        shopDetails.sharedTimeStop = webutil.formatDate(new Date(Date.parse(shopDetails.sharedTimeStop)))
      }else if(type=='4'){ // 丢失物品处理
        shopDetails.lostTime = webutil.formatDate(new Date(Date.parse(shopDetails.lostTime)))
      }

      this.setData({shopDetails:shopDetails,type:type})
      console.log(shopDetails)
      // 获取发布者userInfo
      client({
        url:'user/getUser?openid='+res.data.data.openid,
        method:'GET'
      }).then(res=>{
        // console.log(res.data.data)
        this.setData({userInfo:res.data.data})
      })
    })

  },
  toChat(){
    let fopenid = this.data.userInfo.openid
    let nickName = this.data.userInfo.nickName
    let avatarUrl = this.data.userInfo.avatarUrl
    let friendList = app.globalData.friendList
    let param = {
      'openid':app.globalData.openid,
      'friendOpenid':fopenid,
      'nickName':nickName,
      'avatarUrl':avatarUrl,
      'noRead':0,
      'chat':null
    }
    if(fopenid == app.globalData.openid){
      wx.showToast({
        title: '不能跟自己聊天',
        icon:"none"
      })
    }else{
      wx.showToast({
        title: '正在跳转聊天界面',
        icon:'loading',
        mask:true
      })
      if(friendList.findIndex(item=>item.friendOpenid === fopenid)==-1){
        friendList.unshift(param)
        // 存入全局变量朋友列表
        app.globalData.friendList=friendList
        // 存入数据库
        client({
          url:'news/addFriend',
          data:param,
          method:'POST'
        })
      }
      wx.navigateTo({
        url: '../news/chat/chat?toOpenid='+fopenid+'&&nickName='+nickName,
      })
    }
  }

})