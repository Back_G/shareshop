import {client} from "../../../utils/request"
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    TabCur: 0,
    imgList1:[],
    imgList2:[],
    picker: ['衣裤鞋', '电子设备','化妆品','生活用品','学习类','其他'],
    check:['线上交易','线下交易'],
    girlboy:['男','女'],
    date1: '2021-5-25',
    date2: '2021-5-25',
    date3: '2021-5-25',
    date4: '2021-5-25',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id - 1) * 60
    })
  },

  DateChange1(e) {
    this.setData({
      date1: e.detail.value
    })
  },

  DateChange2(e) {
    this.setData({
      date2: e.detail.value
    })
  },  

  DateChange3(e) {
    this.setData({
      date3: e.detail.value
    })
  },

  DateChange4(e) {
    this.setData({
      date4: e.detail.value
    })
  },

  ChooseImage1() {
    var that = this
    wx.chooseImage({
      count: 3, //默认3
      sizeType: ['compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album','camera'], //从相册选择
      success: (res) => {
        res.tempFilePaths.forEach(v => {
          that.setData({
            imgList1: that.data.imgList1.concat(v)
          });
        })
      }
    });
  },

  ViewImage1(e) {
    wx.previewImage({
      urls: this.data.imgList1,
      current: e.currentTarget.dataset.url
    });
  },

  DelImg1(e) {
    wx.showModal({
      title: '删除',
      content: '确定要删除这张照片？',
      cancelText: '再看看',
      confirmText: '删除',
      success: res => {
        if (res.confirm) {
          this.data.imgList1.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList1: this.data.imgList1,
            shows:false,
            weigui:false,
          })
        }
      }
    })
  },

  ChooseImage2() {
    var that = this
    wx.chooseImage({
      count: 3, //默认3
      sizeType: ['compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album','camera'], //从相册选择
      success: (res) => {
        res.tempFilePaths.forEach(v => {
          that.setData({
            imgList2: that.data.imgList2.concat(v)
          });
        })
      }
    });
  },
  ViewImage2(e) {
    wx.previewImage({
      urls: this.data.imgList2,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg2(e) {
    wx.showModal({
      title: '删除',
      content: '确定要删除这张照片？',
      cancelText: '再看看',
      confirmText: '删除',
      success: res => {
        if (res.confirm) {
          this.data.imgList2.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList2: this.data.imgList2,
          })
        }
      }
    })
  },

  textareaAInput(e) {
    this.setData({
      textareaAValue: e.detail.value
    })
  },

  PickerChange(e) {
    this.setData({
      index1: e.detail.value
    })
  },
  checkChange(e) {
    this.setData({
      index2: e.detail.value
    })
  },
  girlboy(e){
    this.setData({
      index3: e.detail.value
    })
  },

  //共享物品
  submitthing(e){
    var that = this
    if(e.detail.value.itemTitle == ''){
      wx.showToast({
        title: '物品名称',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(this.data.index1 == null){
      wx.showToast({
        title: '未选择分类',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(this.data.index2 == null){
      wx.showToast({
        title: '未选择方式',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(e.detail.value.price == ""){
      wx.showToast({
        title: '未填写价格',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(e.detail.value.userWx ==''){
      wx.showToast({
        title: '微信号未填写',
        icon: 'none',
        duration: 2000,
      })
      return false
    }

    var imgList = that.data.imgList1;
    var img = [];
    wx.showLoading({
      title: '发布中',
    })
    let PromiseArr = [];
    for (var i = 0; i < imgList.length; i++) {
      PromiseArr.push(new Promise((reslove, reject) =>{
        var name = imgList[i].split("/").pop();
        wx.cloud.uploadFile({
          cloudPath: "sharehouse/" + name,
          filePath: imgList[i],
          success: res => {
            var list ={}
            list.imageUrl = res.fileID
            img.push(list);
            reslove();
            wx.hideLoading();
            wx.showToast({
              title: "发布成功",
            })
          },
          fail: err => { 
            wx.hideLoading();
            wx.showToast({
              title: "发布失败",
            })
          }
        })
      }))
    }
    Promise.all(PromiseArr).then(res => {
      if(this.data.index2 ==0){
        var sellOnline =true
      }else{
        var sellOnline =false
      }
      client({
        url:'shared/addItem',//这里放入路径就行，服务器地址公共部分已拼接
        data:{
          shareItem:1,
          openid:app.globalData.openid,
          itemTitle:e.detail.value.itemTitle,
          itemClassification:this.data.picker[this.data.index1],
          price:e.detail.value.price,
          userWx:e.detail.value.userWx,
          sellOnline:sellOnline,
          imgList:img,
          itemDesc:''
        },
        method:'post'
      }).then(res=>{
        if(res.data.code == 200){
        setTimeout(function () {
          //要延时执行的代码
          wx.navigateBack({
            delta: 1
          })
        }, 1000) //延迟时间
      }
    })
    },3000)
  },

  //共享网络
   async submitnetwork(e){
    if(this.data.date1 == "2021-5-25"){
      wx.showToast({
        title: '未选择开通时间',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(this.data.date2 == "2021-5-25"){
      wx.showToast({
        title: '未选择结束时间',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(e.detail.value.sharedPrice ==""){
      wx.showToast({
        title: '未选择价格',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(e.detail.value.worksharedWx ==""){
      wx.showToast({
        title: '未选择微信号',
        icon: 'none',
        duration: 2000,
      })
      return false
    }
    await client({
      url:'shared/addNetwork',//这里放入路径就行，服务器地址公共部分已拼接
      data:{
        openid:app.globalData.openid,
        sharedWx:e.detail.value.worksharedWx,
        sharedTimeStart:this.data.date1,
        sharedTimeStop:this.data.date2,
        sharedPrice:e.detail.value.sharedPrice
      },
      method:'post'
    }).then(res=>{
      setTimeout(function () {
        //要延时执行的代码
        wx.navigateBack({
          delta: 1
        })
      }, 1000) //延迟时间
    })

  },

  // 共享租房
    async submithouse(e){
      var that = this
      if(e.detail.value.sharedAddress == ''){
        wx.showToast({
          title: '地址未填写',
          icon: 'none',
          duration: 2000,
        })
        return false
      }else if(this.data.index3 == null){
        wx.showToast({
          title: '性别未填写',
          icon: 'none',
          duration: 2000,
        })
        return false
      }else if(this.data.date3 == "2021-5-25"){
        wx.showToast({
          title: '未选择开始时间',
          icon: 'none',
          duration: 2000,
        })
        return false
      }else if(this.data.date4 == "2021-5-25"){
        wx.showToast({
          title: '未选择结束时间',
          icon: 'none',
          duration: 2000,
        })
        return false
      }else if(e.detail.value.sharedTimePrice == ""){
        wx.showToast({
          title: '未填写价格',
          icon: 'none',
          duration: 2000,
        })
        return false
      }else if(e.detail.value.housesharedWx ==''){
        wx.showToast({
          title: '微信号未填写',
          icon: 'none',
          duration: 2000,
        })
        return false
      }
      var imgList = that.data.imgList2;
      var img = [];
      wx.showLoading({
        title: '发布中',
      })
      let PromiseArr = [];
      for (var i = 0; i < imgList.length; i++) {
        PromiseArr.push(new Promise((reslove, reject) =>{
          var name = imgList[i].split("/").pop();
          wx.cloud.uploadFile({
            cloudPath: "sharehouse/" + name,
            filePath: imgList[i],
            success: res => {
              var list ={}
              list.imageUrl = res.fileID
              img.push(list);
              reslove();
              wx.hideLoading();
              wx.showToast({
                title: "发布成功",
              })
            },
            fail: err => { 
              wx.hideLoading();
              wx.showToast({
                title: "发布失败",
              })
            }
          })
        }))
      }
      Promise.all(PromiseArr).then(res => {
      client({
          url:'shared/addRental',//这里放入路径就行，服务器地址公共部分已拼接
          data:{
            "openid":app.globalData.openid,
            "sharedWx":e.detail.value.housesharedWx,
            "sharedSex":this.data.girlboy[this.data.index3],
            "sharedTimeStart":this.data.date3,
            "sharedTimeStop":this.data.date4,
            "sharedPrice":parseFloat(e.detail.value.sharedTimePrice).toFixed(2),
            "sharedAddress":e.detail.value.sharedAddress,
            "imgList":img
          },
          method:'post'
        }).then(res=>{
          if(res.data.code == 200){
            setTimeout(function () {
              //要延时执行的代码
              wx.navigateBack({
                delta: 1
              })
            }, 1000) //延迟时间
          }
        })
    },3000)  
  },
})