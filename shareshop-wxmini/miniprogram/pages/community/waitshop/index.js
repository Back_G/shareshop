import {client} from "../../../utils/request"
const app = getApp()

Page({
  data: {
    imgList:[],
    picker: ['衣裤鞋', '电子设备','化妆品','生活用品','学习类','其他'],
    check:['线上交易','线下交易'],
  },
  onLoad: function (options) {

  },
  ChooseImage() {
    var that = this
    wx.chooseImage({
      count: 3, //默认3
      sizeType: ['compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album','camera'], //从相册选择
      success: (res) => {
        res.tempFilePaths.forEach(v => {
          that.setData({
            imgList: that.data.imgList.concat(v)
          });
        })
      }
    });
  },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '删除',
      content: '确定要删除这张照片？',
      cancelText: '再看看',
      confirmText: '删除',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList,
            shows:false,
            weigui:false,
          })
        }
      }
    })
  },
  textareaAInput(e) {
    this.setData({
      textareaAValue: e.detail.value
    })
  },

  PickerChange(e) {
    this.setData({
      index1: e.detail.value
    })
  },
  checkChange(e) {
    this.setData({
      index2: e.detail.value
    })
  },
  async submitComment(e){
      var that = this
    if(e.detail.value.itemTitle == ''){
      wx.showToast({
        title: '物品名称',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(this.data.index1 == null){
      wx.showToast({
        title: '未选择分类',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(this.data.index2 == null){
      wx.showToast({
        title: '未选择方式',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(e.detail.value.price == ""){
      wx.showToast({
        title: '未填写价格',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(e.detail.value.userWx ==''){
      wx.showToast({
        title: '微信号未填写',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(this.data.textareaAValue == undefined && this.data.imgList.length == 0){
      wx.showToast({
        title: '写点什么吧',
        icon: 'none',
        duration: 2000,
      })
      return false
    }

    var imgList = that.data.imgList;
    var img = [];
    wx.showLoading({
      title: '发布中',
    })
    let PromiseArr = [];
    for (var i = 0; i < imgList.length; i++) {
      PromiseArr.push(new Promise((reslove, reject) =>{
        var name = imgList[i].split("/").pop();
        wx.cloud.uploadFile({
          cloudPath: "sharehouse/" + name,
          filePath: imgList[i],
          success: res => {
            var list ={}
            list.imageUrl = res.fileID
            img.push(list);
            reslove();
            wx.hideLoading();
            wx.showToast({
              title: "发布成功",
            })
          },
          fail: err => { 
            wx.hideLoading();
            wx.showToast({
              title: "发布失败",
            })
          }
        })
      }))
    }
    Promise.all(PromiseArr).then(res => {
      if(this.data.index2 ==0){
        var sellOnline =true
      }else{
        var sellOnline =false
      }
      client({
        url:'shared/addItem',//这里放入路径就行，服务器地址公共部分已拼接
        data:{
          shareItem:0,
          openid:app.globalData.openid,
          itemTitle:e.detail.value.itemTitle,
          itemClassification:this.data.picker[this.data.index1],
          price:e.detail.value.price,
          userWx:e.detail.value.userWx,
          sellOnline:sellOnline,
          imgList:img,
          itemDesc:this.data.textareaAValue
        },
        method:'post'
      }).then(res=>{
        if(res.data.code == 200){
        setTimeout(function () {
          //要延时执行的代码
          wx.navigateBack({
            delta: 1
          })
        }, 1000) //延迟时间
      }
    })
    },3000)
  }
})