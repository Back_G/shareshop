const app = getApp()
import {client} from "../../../utils/request"
Page({
  data: {
    imgList:[],
    picker: ['无偿','有偿'],
    date: '2021-5-25',

  },
  onLoad: function (options) {

  },
  DateChange1(e) {
    this.setData({
      date: e.detail.value
    })
  },
  ChooseImage() {
    var that = this
    wx.chooseImage({
      count: 3, //默认3
      sizeType: ['compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album','camera'], //从相册选择
      success: (res) => {
        res.tempFilePaths.forEach(v => {
          that.setData({
            imgList: that.data.imgList.concat(v)
          });
        })
      }
    });
  },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '删除',
      content: '确定要删除这张照片？',
      cancelText: '再看看',
      confirmText: '删除',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList,
            shows:false,
            weigui:false,
          })
        }
      }
    })
  },
  textareaAInput(e) {
    this.setData({
      textareaAValue: e.detail.value
    })
  },

  PickerChange(e) {
    this.setData({
      index: e.detail.value
    })
  },

  async submitComment(e){
    var that = this
    if(e.detail.value.itemsName == ''){
      wx.showToast({
        title: '丢失物品未填写',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(this.data.index == null){
      wx.showToast({
        title: '是否有偿',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(this.data.date == "2021-5-25"){
      wx.showToast({
        title: '未选择丢失时间',
        icon: 'none',
        duration: 2000,
      })
      return false
    }else if(this.data.userWx == "2021-5-25"){
      wx.showToast({
        title: '未输入微信号码',
        icon: 'none',
        duration: 2000,
      })
      return false
    }
    var imgList = that.data.imgList;
    var img = [];
    wx.showLoading({
      title: '发布中',
    })
    let PromiseArr = [];
    for (var i = 0; i < imgList.length; i++) {
      PromiseArr.push(new Promise((reslove, reject) =>{
        var name = imgList[i].split("/").pop();
        // console.log(imgList[i])
        wx.cloud.uploadFile({
          cloudPath: "lostthing/" + name,
          filePath: imgList[i],
          success: res => {
            var list ={}
            list.imageUrl = res.fileID
            img.push(list);
            reslove();
            wx.hideLoading();
            wx.showToast({
              title: "发布成功",
            })
          },
          fail: err => { 
            // console.log(err)
            wx.hideLoading();
            wx.showToast({
              title: "发布失败",
            })
          }
        })
      }))
    }
    Promise.all(PromiseArr).then(res => {
      client({
          url:'lostitems/addLostItems',
          data:{
            "openid":app.globalData.openid,
            "userWx":e.detail.value.userWx,
            "itemsName":e.detail.value.itemsName,
            "isItPaid":this.data.index,
            "lostTime":this.data.date,
            "imgList":img
          },
          method:'post'
        }).then(res=>{
          if(res.data.code == 200){
            setTimeout(function () {
              //要延时执行的代码
              wx.navigateBack({
                delta: 1
              })
            }, 1000) //延迟时间
          }
        })
    },3000)  
  }
})