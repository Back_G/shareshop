import {client} from "../../utils/request"
Page({
  data: {
    networkimg:{imageUrl:"../../images/network.jpg"},
    choose:[
      {name:"衣裤鞋",image:"../../images/clothes.png"},
      {name:"电子设备",image:"../../images/dianzi.png"},
      {name:"化妆品",image:"../../images/kouhong.png"},
      {name:"生活用品",image:"../../images/life.png"},
      {name:"学习类",image:"../../images/study.png"},
      {name:"共享网络",image:"../../images/usetwo.png"},
      {name:"共享租房",image:"../../images/shareeach.png"},
      {name:"其他",image:"../../images/other.png"}
  ],
    page: 1,
    pageSize: 10,
    hasMoreData: true,
    contentlist: [],
  },
  onLoad: function() {

  },
  onShow: function () {
    wx.cloud.callFunction({
      name:"get_firstcarousel"
    }).then(res=>{
      // console.log(res)
      this.setData({
        indeximg:res.result.data
      })
    })
    this.setData({
      page:1
    })
    this.getList()
    this.tabBar() ;
  },
  tabBar(){
    if(typeof this.getTabBar === 'function' && this.getTabBar()){
      this.getTabBar().setData({
        selected:0
      })
    }
  },
  async getList() {
    var that = this
    await client({
      url:'shared/getPageSharedInfo?page='+ that.data.page+'&size=10',
      method:'get'
    }).then(res=>{
      var contentlistTem = that.data.contentlist
      if (res.data.code == 200) {
        if (that.data.page == 1) {
          contentlistTem = []
        }
        var contentlist = res.data.data.records
        for(let i in contentlist){
          if(contentlist[i].lostitems != null){
            contentlist[i].price = ''
            contentlist[i].itemClassification ="丢失物品"
            contentlist[i].title = contentlist[i].lostitems.itemsName
          }
          if(contentlist[i].imgList.length == 0){
            contentlist[i].imgList.push(this.data.networkimg)
          }
          if(contentlist[i].sellOnline == 1){
            contentlist[i].sellOnline ="线上"
          }else if(contentlist[i].sellOnline == 0){
            contentlist[i].sellOnline = "线下"
          }
        }
        if (contentlist.length < that.data.pageSize) {
          that.setData({
            contentlist: contentlistTem.concat(contentlist),
            hasMoreData: false
          })
        } else {
          that.setData({
            contentlist: contentlistTem.concat(contentlist),
            hasMoreData: true,
            page: that.data.page + 1
          })
        }
        wx.hideLoading();
        //隐藏导航条加载动画
        wx.hideNavigationBarLoading();
        //停止下拉刷新
        wx.stopPullDownRefresh();
      }
    })
  },
  // 跳转分类页面
  toClassification(e){
    wx.navigateTo({
      url: 'classification/classification?name='+e.currentTarget.dataset.name,
    })
  },
  onReachBottom: function() {
    if (this.data.hasMoreData) {
      this.getList()
    } else {
      wx.showToast({
        title: '没有更多数据',
      })
    }
  },
  onPullDownRefresh: function () {
    this.onRefresh();
  },
  onRefresh(){
    wx.showNavigationBarLoading(); 
    wx.showLoading({
      title: '刷新中...',
    })
    this.setData({
      page:1
    })
    this.getList();
  },
})
