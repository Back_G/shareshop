const app = getApp()
const {client}  = require("../../../utils/request")
const webutil  = require("../../../utils/webutil")

var page = 1
var size = 500

Page({
  data:{
    type:'',
    shopList:[]
  },

  onLoad(option){
    this.setData({type:option.name})
    if(option.name == "共享网络"){
      this.getShareNetwork()
    }else if(option.name=="共享租房"){
      this.getRental()
    }else{
      this.getItemsByClassification(option.name)
    }
  },
  // 获取共享网络
  getShareNetwork(){
    client({
      url:'shared/getNetwork?page='+page+'&&size='+size,
      method:'GET'
    }).then(res=>{
      console.log(res)
      wx.showToast({
        title: '共有'+res.data.data.total+'条数据',
        icon:'none'
      })
      this.setData({
        shopList:res.data.data.records.reverse()
      })
      // console.log(this.data.shopList)
    })
  },

  // 获取共享租房
  getRental(){
    client({
      url:'shared/getRental?page='+page+'&&size='+size,
      method:'GET'
    }).then(res=>{
      wx.showToast({
        title: '共有'+res.data.data.total+'条数据',
        icon:'none'
      })
      this.setData({
        shopList:res.data.data.records.reverse()
      })
      // console.log(this.data.shopList)
    })
  },

  // 获取共享、闲置物品by类别
  getItemsByClassification(classification){
    client({
      url:'shared/getItemsByClassification?classification='+classification,
      method:'GET'
    }).then(res=>{
      wx.showToast({
        title: '共有'+res.data.data.length+'条数据',
        icon:'none'
      })
      this.setData({
        shopList:res.data.data.reverse()
      })
      // console.log(this.data.shopList)
    })
  },

  toDetail(e){
    let id = e.currentTarget.dataset.id
    let type = e.currentTarget.dataset.type

    wx.navigateTo({
      url: '/pages/shopdetail/shopdetail?id='+id+'&&type='+type,
    })
  }
})