const ws = require("./utils/websocket")
const {client}  = require("./utils/request")
App({
  onLaunch: function () {
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        env: 'shareshop-9g31002t1f10c0a8',
        traceUser: true,
      })
    }

    if(wx.getStorageSync("openid")!=null&&wx.getStorageSync("openid")!=''){
      let openid = wx.getStorageSync('openid')
      this.toConnect(openid)
    }else{
      wx.cloud.callFunction({
        name:"get_openid",
      }).then(res=>{
        let openid = res.result.openid
        wx.setStorageSync("openid",openid)
        this.toConnect(openid)
      })
    }
  },
  toConnect(openid){
    this.globalData.openid = openid
    // var url = 'ws://1.116.194.164:8080/ws/'+ openid;
    var url = 'ws://192.168.1.107:8080/ws/'+ openid;
    ws.connect(null,url)

    // 获取朋友列表
    if(this.globalData.friendList ==null){
      client({
        url:'news/getFriends?openid='+openid,
        method:'GET'
      }).then(res=>{
        this.globalData.friendList = res.data.data
      })
    }
   
  },
  globalData: {
    openid:'',
    friendList:null
  }
})
