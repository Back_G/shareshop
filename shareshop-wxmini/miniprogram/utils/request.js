//定义服务器地址公共部分
// const baseUrl = 'http://1.116.194.164:8080/';
const baseUrl = 'http://192.168.1.107:8080/';

/**
 * 参数说明
   parmas:{},是一个对象
 */

export const client = (parmas) => {
  if(parmas.method === undefined) {//若是没有传请求方式，默认get
    parmas.method = 'get';//设置默认值
    parmas.header = {'content-type': 'application/json'};//设置默认值
  }

  return new Promise((resolve,reject) => {
    wx.request({
      ...parmas,//这里就是调用传来的对象解构
      url: baseUrl+parmas.url,
      success:res => {
        resolve(res)
      },
      fail:err => {
        reject(err)
      }
    });
  })
};