// var url = 'ws://192.168.1.107:8080/ws';
// var utils = require('./util.js');

function connect(user, url, func) {
    wx.connectSocket({
        url: url,
        header: {'content-type': 'application/json'},
        success: function () {
            console.log('服务器连接成功')
        },
        fail: function () {
            console.log('服务器连接失败')
            wx.showToast({
              title: '服务器连接失败',
              icon: "none",
              duration: 2000
            })
        }
    })
    wx.onSocketOpen(function (res) {
        console.log("服务器open")
        //接受服务器消息
        // wx.onSocketMessage(func);//func回调可以拿到服务器返回的数据
    });
    wx.onSocketError(function (res) {
      wx.showToast({
        title: '服务器连接成功',
        icon: "none",
        duration: 2000
      })
        // wx.showToast({
        //     title: '信道连接失败，请检查！',
        //     icon: "none",
        //     duration: 2000
        // })
    })
}

//发送消息
function send(msg) {
    wx.sendSocketMessage({
        data: msg
    });
}

module.exports = {
    connect: connect,
    send: send
}
