Component({
  properties: {

  },
  data: {
    selected:0,
    tabList:[
      {
        "pagePath": "pages/index/index",
        "text": "首页"
      },
      {
        "pagePath": "pages/release/release/index",
        "text": "发布"
      },
      {
        "pagePath": "pages/community/community/index",
        "text": "社区"
      },
      {
        "pagePath": "pages/news/news/index",
        "text": "消息"
      },
      {
        "pagePath": "pages/mine/mine/mine",
        "text": "我的"
      }
    ]
  },
  methods: {
    switchTab(e){
      // console.log('111')
      // console.log(this.data)
      let key = Number(e.currentTarget.dataset.index);
      let tabList = this.data.tabList;
      let selected = this.data.selected;

      if(selected !== key){
        this.setData({
          selected:key
        });
        wx.switchTab({
          url: `/${tabList[key].pagePath}`,
        })
      }
    }
  }
})
