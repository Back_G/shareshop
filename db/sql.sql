/*
Navicat MySQL Data Transfer

Source Server         : 1.116.194.164
Source Server Version : 50734
Source Host           : 1.116.194.164:3306
Source Database       : weixinapp

Target Server Type    : MYSQL
Target Server Version : 50734
File Encoding         : 65001

Date: 2021-06-08 20:43:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_chat
-- ----------------------------
DROP TABLE IF EXISTS `t_chat`;
CREATE TABLE `t_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `from_openid` varchar(60) NOT NULL COMMENT '发送者的openid',
  `to_openid` varchar(60) NOT NULL COMMENT '接收者的openid',
  `content` varchar(255) DEFAULT NULL COMMENT '聊天内容',
  `create_time` varchar(30) NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_friend
-- ----------------------------
DROP TABLE IF EXISTS `t_friend`;
CREATE TABLE `t_friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `openid` varchar(100) NOT NULL COMMENT 'openid',
  `friend_openid` varchar(100) NOT NULL COMMENT '朋友openid',
  `no_read` int(11) DEFAULT '0' COMMENT '未读信息，默认0',
  `chat` varchar(255) DEFAULT NULL COMMENT '最后一条消息，content字段和createTime字段',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_idleItems
-- ----------------------------
DROP TABLE IF EXISTS `t_idleItems`;
CREATE TABLE `t_idleItems` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '闲置物品id，主键',
  `openid` varchar(100) NOT NULL COMMENT '发布用户openid',
  `item_title` varchar(50) NOT NULL COMMENT '物品标题',
  `item_desc` varchar(1000) DEFAULT NULL COMMENT '物品描述',
  `item_classification` varchar(20) DEFAULT '' COMMENT '物品分类',
  `sell_online` tinyint(4) NOT NULL COMMENT '是否在线交易',
  `price` double(11,2) DEFAULT '0.00' COMMENT '物品价格',
  `user_wx` varchar(50) DEFAULT NULL COMMENT '联系微信号',
  `is_share_item` tinyint(4) NOT NULL COMMENT '是否共享物品',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '加入时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1399094852629856258 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='发布闲置物品';

-- ----------------------------
-- Table structure for t_image
-- ----------------------------
DROP TABLE IF EXISTS `t_image`;
CREATE TABLE `t_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `relation_id` bigint(20) NOT NULL COMMENT '关联id',
  `image_url` varchar(255) NOT NULL COMMENT '图片地址',
  `relation_type` int(11) NOT NULL COMMENT '关联的类型（共享、闲置、二手...）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_lostItems
-- ----------------------------
DROP TABLE IF EXISTS `t_lostItems`;
CREATE TABLE `t_lostItems` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `lost_time` datetime DEFAULT NULL COMMENT '丢失时间',
  `items_name` varchar(20) DEFAULT NULL COMMENT '物品名称',
  `openid` varchar(150) DEFAULT NULL COMMENT '发布者openid',
  `user_wx` varchar(20) DEFAULT NULL COMMENT '联系微信号',
  `is_it_paid` tinyint(4) DEFAULT '0' COMMENT '是否有偿',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '加入时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='丢失物品';

-- ----------------------------
-- Table structure for t_shared_info
-- ----------------------------
DROP TABLE IF EXISTS `t_shared_info`;
CREATE TABLE `t_shared_info` (
  `id` bigint(20) NOT NULL,
  `create_tm` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` tinyint(4) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_sharedNetwork
-- ----------------------------
DROP TABLE IF EXISTS `t_sharedNetwork`;
CREATE TABLE `t_sharedNetwork` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '共享ID',
  `openid` varchar(100) NOT NULL,
  `shared_time_start` datetime DEFAULT NULL COMMENT '网络共享开始时间',
  `shared_time_stop` datetime DEFAULT NULL COMMENT '网络共享结束时间',
  `shared_price` double(10,2) DEFAULT '0.00' COMMENT '共享网络价格',
  `shared_wx` varchar(20) DEFAULT NULL COMMENT '联系微信号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '加入时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1399107326673162243 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='共享网络';

-- ----------------------------
-- Table structure for t_sharedRental
-- ----------------------------
DROP TABLE IF EXISTS `t_sharedRental`;
CREATE TABLE `t_sharedRental` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '合租ID，主键',
  `shared_time_start` datetime DEFAULT NULL COMMENT '合租开始时间',
  `shared_time_stop` datetime DEFAULT NULL COMMENT '合租结束时间',
  `shared_price` double(10,2) DEFAULT '0.00' COMMENT '合租价格',
  `shared_sex` varchar(10) DEFAULT NULL COMMENT '合租性别',
  `shared_address` varchar(100) DEFAULT NULL COMMENT '地址',
  `openid` varchar(100) NOT NULL COMMENT '发布者openid',
  `shared_wx` varchar(20) DEFAULT NULL COMMENT '联系微信号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '加入时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1399091290331971587 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='共享租房';

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `openid` varchar(50) NOT NULL COMMENT '用户openid',
  `nick_name` varchar(15) NOT NULL DEFAULT '' COMMENT '用户名',
  `avatar_url` varchar(255) NOT NULL COMMENT '头像',
  `gender` int(4) DEFAULT NULL COMMENT '用户性别',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `language` varchar(50) DEFAULT '' COMMENT '用户语言',
  `city` varchar(50) DEFAULT NULL COMMENT '用户城市',
  `province` varchar(50) DEFAULT NULL COMMENT '用户省份',
  `country` varchar(50) DEFAULT NULL COMMENT '用户国家',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '加入时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`openid`),
  UNIQUE KEY `user_name` (`nick_name`) USING BTREE,
  KEY `user_email` (`gender`) USING BTREE,
  KEY `user_create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户表';
