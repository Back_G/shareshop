本项目是前后分离项目，前端采用小程序端，后端采用springboot框架，使用数据库mysql。
### 项目介绍
请见项目文档

### 技术简介
- 后端：springboot、mysql、websocket等
- 前端：微信小程序、colorui、云开发

### 目录结构
- db 数据库文件
- doc 项目文档
- shareshop-java 后端项目
- shareshop-wxmini 小程序项目

### 小程序体验版二维码
![小程序体验版二维码](https://images.gitee.com/uploads/images/2021/0608/210808_9dfdd284_5644876.png "微信截图_20210608210741.png")